import axios from "axios";
import React, { Component } from "react";

class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userExists: false,
            userCreated: false,
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit = (event) => {
        this.setState({ userExists: false, userCreated: false });

        event.preventDefault();
        const data = new FormData(event.target);
        let user = {};
        for (var [key, value] of data.entries()) {
            user[key] = value;
        }
        axios({
            method: "post",
            url: "https://intense-caverns-45038.herokuapp.com/register",
            data: user,
        })
            .then((res) => {
                if (res.data.message === "userExists") {
                    this.setState({ userExists: true });
                } else if (res.data.message === "userCreated") {
                    console.log(res.data.message);
                    this.setState({ userCreated: true });
                }
            })
            .catch((err) => {
                console.log(err);
            });
    };
    render() {
        return (
            <form
                className="register-form"
                onSubmit={(e) => this.handleSubmit(e)}
            >
                <div className="form-group">
                    <label htmlFor="firstName">Firstname</label>
                    <input
                        type="text"
                        className="form-control"
                        id="firstname"
                        placeholder=" Enter firstname"
                        name="firstName"
                        required
                    />

                    <label htmlFor="lastname">Lastname</label>
                    <input
                        type="text"
                        className="form-control"
                        id="lastName"
                        placeholder="Enter lastname"
                        name="lastName"
                        required
                    />

                    <label htmlFor="email">Email address</label>
                    <input
                        type="email"
                        className="form-control"
                        id="email"
                        name="email"
                        placeholder="Enter email"
                        required
                    />

                    <label htmlFor="password">Password</label>
                    <input
                        type="password"
                        className="form-control"
                        id="password"
                        placeholder="Password"
                        name="password"
                        required
                    />

                    <button
                        type="submit"
                        className="btn btn-primary btn-block"
                        id="register-submit"
                        style={{ marginTop: 10, width: "100%" }}
                    >
                        Submit
                    </button>
                </div>

                <div className="invalid-feedback d-block">
                    {this.state.userExists && "User Already exists"}
                </div>
                <div className="valid-feedback d-block">
                    {this.state.userCreated && "New user created sucessfully"}
                </div>
            </form>
        );
    }
}

export default Signup;
