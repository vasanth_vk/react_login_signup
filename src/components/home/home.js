import React, { Component } from "react";
class Home extends Component {
    handleLogin = () => {
        this.props.handleNavbar({
            loginActive: true,
            signupActive: false,
            homeActive: false,
        });
    };
    handleSignup = () => {
        this.props.handleNavbar({
            signupActive: true,
            loginActive: false,
            homeActive: false,
        });
    };
    render() {
        return (
            <div className="d-flex flex-column">
                <h1>HOMEPAGE</h1>
                <button
                    type="button"
                    className="btn btn-primary"
                    onClick={this.handleLogin}
                    style={{ margin: 10 }}
                >
                    LOGIN
                </button>
                <button
                    type="button"
                    className="btn btn-primary"
                    onClick={this.handleSignup}
                    style={{ margin: 10 }}
                >
                    SIGNUP
                </button>
            </div>
        );
    }
}

export default Home;
